import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TryDemoComponent } from './try-demo.component';

describe('TryDemoComponent', () => {
  let component: TryDemoComponent;
  let fixture: ComponentFixture<TryDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TryDemoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TryDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
