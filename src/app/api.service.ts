
import {map} from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';

/*
  Generated class for the Api provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ApiService {

  //DATABASES
  //url = 'http://localhost:3000/';//local
  //url = "https://renter-app-api.herokuapp.com/";//STAGING
  url = "https://livingsqr-production.herokuapp.com/"//PRODUCTION

  private authenticatedToken:any = null;

  constructor(public http: Http) {
  }

  setAuthToken(auth_token){
    this.authenticatedToken = auth_token;
  }
  
  getAuthHeader(){
    let authenticatedHeaders = new Headers({ 'Content-Type': 'application/json' });
    if(this.authenticatedToken){
      authenticatedHeaders.append('Authorization', this.authenticatedToken);
    } else {
      authenticatedHeaders.append('Authorization', null);
    }
    return authenticatedHeaders;
  }

  post(params:any, to:string){
    let headers = new Headers();
    return new Promise((resolve, reject)=>{
      console.log("POST: " + this.url+to);
      this.http.post(this.url+to, params,
      new RequestOptions({ headers: this.getAuthHeader()})).pipe(map(res => res.json())).subscribe(data =>{
        resolve(<any>data);
      }, (error)=>{
        console.log(JSON.stringify(error.json()));
        reject(error.json());
      })
      
    }); 
  }

  /**
   * 
   * @param to table to get
   * @param id optional id (if not specified, gets all)
   */
  get(to: string, id = null){
    return new Promise((resolve, reject)=>{
      let id_string = id ? ("/" + id) : "";
      console.log("GET: " + this.url + to + id_string);
      this.http.get(this.url + to + id_string, new RequestOptions({ headers: this.getAuthHeader()})).pipe(
      map((res: Response) => res.json()))
        .subscribe(
          data => {
            resolve(data);
         }, reject);
    })
  }

  put(params:any, to:string, id:number){
    let headers = new Headers();
    return new Promise((resolve, reject)=>{
      console.log("PUT: " + this.url+to+ "/" + id);
      this.http.put(this.url + to + "/" + id, params,//.json
        new RequestOptions({ headers: this.getAuthHeader()})).pipe(map(res => res.json())).subscribe(data =>{
        console.log("data passed fine");
        resolve(data);
      }, (error)=>{
        console.log(JSON.stringify(error.json()));
        reject(error);
      })
      
    }); 
  }

  ///Needs more thought about how to do params
  delete(to:string, id:number){
    let headers = new Headers();
    return new Promise((resolve, reject)=>{
      console.log("DELETE: "+ (this.url+to + "/" +id));
      this.http.delete(this.url+to+ "/" +id,//.json
        new RequestOptions({ headers: this.getAuthHeader()})).pipe(map(res => res.json())).subscribe(data =>{
        resolve(data);
      }, (error)=>{
        console.log(JSON.stringify(error.json()));
        reject(error);
      })
      
    }); 
  }


}
