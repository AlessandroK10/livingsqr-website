import { TestBed, inject } from '@angular/core/testing';

import { EmailHandlerService } from './email-handler.service';

describe('EmailHandlerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmailHandlerService]
    });
  });

  it('should be created', inject([EmailHandlerService], (service: EmailHandlerService) => {
    expect(service).toBeTruthy();
  }));
});
