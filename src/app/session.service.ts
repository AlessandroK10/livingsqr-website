import { Injectable } from '@angular/core';

@Injectable()
export class SessionService {

  private currentInfo = "landlord";
  private routeFragment = "";

  constructor() { }

  /**
   * userType: either 'landlord' or 'tenant'
   */
  setCurrentInfo(userType){
    this.currentInfo = userType;
  }
  getCurrentInfo(){
    return this.currentInfo;
  }

  setRouteFragment(fragment: string){
    this.routeFragment = fragment;
  }

  getRouteFragment(){
    let returnVal = this.routeFragment;
    this.routeFragment = null;
    return returnVal;
  }


}
