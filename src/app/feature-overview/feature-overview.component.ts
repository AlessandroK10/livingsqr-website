import { Component, OnInit } from '@angular/core';
import {SessionService} from '../session.service';



@Component({
  selector: 'app-feature-overview',
  templateUrl: './feature-overview.component.html',
  styleUrls: ['./feature-overview.component.scss']
})
export class FeatureOverviewComponent implements OnInit {

  constructor(public session: SessionService) { }

  ngOnInit() {

  }
}
