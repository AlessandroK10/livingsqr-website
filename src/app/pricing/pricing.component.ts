import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.scss']
})
export class PricingComponent implements OnInit {

  slideValue = 50;
  constructor() { }

  ngOnInit() {
  }

  pricePerUnit(){
    return (this.getPrice()/this.slideValue*1.0).toFixed(2);;
  }

  getPrice(){
    if(this.slideValue <= 20){
      return 20;
    }
    if(this.slideValue > 20 && this.slideValue <= 200){
      return Math.round((20 + (this.slideValue - 20)*(8.0/18.0)));
    }
    if(this.slideValue > 200 && this.slideValue <= 250){
      return Math.round(100 +(this.slideValue - 200)*0.4);
    }
    return -1
  }

}
