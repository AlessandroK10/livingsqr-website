import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {EmailHandlerService} from '../email-handler.service';

@Component({
  selector: 'app-validate',
  templateUrl: './validate.component.html',
  styleUrls: ['./validate.component.scss']
})
export class ValidateComponent implements OnInit {

  id = "";
  status = "";

  constructor(private route: ActivatedRoute,
              private emailHandler: EmailHandlerService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.emailHandler.validateEmail(this.id).then(res =>{
      this.status = "validated";
      //good, email is validated
    }, err =>{
      if(err['status'] && err['status'] == 'expired'){
        this.status = "expired";
        //display expired status
      }else{
        this.status = "error";
        //display something went wrong status
      }
    })
  }

}
