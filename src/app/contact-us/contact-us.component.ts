import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {EmailHandlerService} from '../email-handler.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {

  contactForm: FormGroup;
  successfullySent = false;
  emailSentTo = "";
  submit_once = false;

  loading = false;

  constructor(public emailHandler: EmailHandlerService) {
    this.contactForm = new FormGroup({
      'email': new FormControl('', [Validators.required]),
      'first_name': new FormControl('', [Validators.required]),
      'last_name': new FormControl('', []),
      'subject': new FormControl('', [Validators.required]),
      'message': new FormControl('', [Validators.required])
    });
  }

  ngOnInit() {
  }

  onSubmit(){
    console.log("submitted");
    this.submit_once = true;
    if(this.contactForm.valid){
      let data = {};
      data['email'] = this.contactForm.get('email').value;
      data['first_name'] = this.contactForm.get('first_name').value;
      data['last_name'] = this.contactForm.get('last_name').value;
      data['subject'] = this.contactForm.get('subject').value;
      data['message'] = this.contactForm.get('message').value;
      this.loading = true;
      this.emailHandler.contactUs(data).then(res =>{
        this.successfullySent = true;
        this.loading = false;
        this.emailSentTo = this.contactForm.get('email').value;
      }, err =>{
        this.loading = true;
      })
    }
  }

}
