import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {PasswordResetComponent} from './password-reset/password-reset.component';
import {ValidateComponent} from './validate/validate.component';
import {PaymentInfoComponent} from './payment-info/payment-info.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {ContactUsComponent} from './contact-us/contact-us.component';
import {AboutComponent} from './about/about.component';
import {SponsorsComponent} from './sponsors/sponsors.component';
import {PrivacyPolicyComponent} from './privacy-policy/privacy-policy.component';
import {TermsComponent} from './terms/terms.component';
import {GetStartedComponent} from './get-started/get-started.component';
import {PricingComponent} from './pricing/pricing.component';
import {TryDemoComponent} from './try-demo/try-demo.component';
import {LandlordRegisterComponent} from './landlord-register/landlord-register.component';
import {ResidenceComponent} from './residence/residence.component';

const routes: Routes = [
  //{ path: 'home', component: HomeComponent },
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'password-reset/:id', component: PasswordResetComponent },   
  { path: 'validate/:id', component: ValidateComponent },
  { path: 'payment', component: PaymentInfoComponent },
  { path: 'pricing', component: PricingComponent },
  { path: 'password-reset/:id', component: PasswordResetComponent }, 
  { path: 'contact', component: ContactUsComponent }, 
  { path: 'about', component: AboutComponent }, 
  { path: 'privacy-policy', component: PrivacyPolicyComponent },   
  { path: 'partners-sponsors', component: SponsorsComponent }, 
  { path: 'terms', component: TermsComponent },   
  { path: 'get-started', component: GetStartedComponent }, 
  { path: 'sales', component: LandlordRegisterComponent },   
  { path: 'try-demo', component: TryDemoComponent },
  { path: 'residence', component: ResidenceComponent },   
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
