import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {TranslateService} from 'ng2-translate';
import {EmailHandlerService} from '../email-handler.service';

@Component({
  selector: 'app-landlord-register',
  templateUrl: './landlord-register.component.html',
  styleUrls: ['./landlord-register.component.scss']
})
export class LandlordRegisterComponent implements OnInit {

  registerForm: FormGroup;
  successfullySent = false;
  emailSentTo = "";
  submit_once = false;

  loading = false;

  constructor(public emailHandler: EmailHandlerService, public translate: TranslateService) {

    this.registerForm = new FormGroup({
      'email': new FormControl('', [Validators.required]),
      'first_name': new FormControl('', [Validators.required]),
      'last_name': new FormControl('', []),
      'country': new FormControl('', [Validators.required]),
      'city': new FormControl('', [Validators.required]),
      'num_buildings': new FormControl('', [Validators.required]),
      'num_units': new FormControl('', [Validators.required]),
      'comments': new FormControl('', [])
    });
  }

  ngOnInit() {
  }

  onSubmit(){
    console.log("submitted");
    this.submit_once = true;
    if(this.registerForm.valid){
      let data = {};
      data['email'] = this.registerForm.get('email').value;
      data['first_name'] = this.registerForm.get('first_name').value;
      data['last_name'] = this.registerForm.get('last_name').value;
      data['country'] = this.registerForm.get('country').value;
      data['city'] = this.registerForm.get('city').value;
      data['num_buildings'] = this.registerForm.get('num_buildings').value + "";
      data['num_units'] = this.registerForm.get('num_units').value + "";
      data['comments'] = this.registerForm.get('comments').value;
      data['language'] = this.translate.currentLang;
      this.loading = true;
      this.emailHandler.registerLandlord(data).then(res=>{
        //clear form and say message that it was sent
        this.emailSentTo = this.registerForm.get('email').value;
        this.successfullySent = true;
        this.registerForm.reset();
        this.loading = false;
        console.log("email was sent!");
      }, err =>{
        //send error message
        this.loading = false;
        console.log("email did not send!");
      })
    }
  }

}
