import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { SessionService } from '../session.service';
import {TranslateService} from 'ng2-translate';
import { DomSanitizer } from '@angular/platform-browser';
import { trigger, state, style, animate, transition } from '@angular/animations';



@Component({
  selector: 'app-home',
  animations: [
    trigger('initialAnimation', [
      state('open', style({
        opacity: '1',
      })),
      state('closed', style({
        opacity: '0',
        display: 'none'
      })),
      transition('open => closed', [
        animate('1s')
      ]),
      transition('closed => open', [
        animate('1s')
      ]),
    ]),
  ],
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  private fragment: string;
  featuresTitle = "Landlord";
  language = "en"
  safeURL;
  innerWidth:any;
  hideHome = true;
  
  constructor(public route: ActivatedRoute,
              public session: SessionService,
              public translate: TranslateService,
              private _sanitizer: DomSanitizer) {
                //this.safeURL = this._sanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/8ObyPq-OmO0");
                this.safeURL = this._sanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/0aMkcPZSRcs");
                console.log("current language: " + this.translate.currentLang.substr(0,2));
                this.translate.get("HOME." +this.session.getCurrentInfo().toLocaleUpperCase()).subscribe(text =>{this.featuresTitle = text;})
                this.translate.onLangChange.subscribe(_=>{
                  this.translate.get("HOME." +this.session.getCurrentInfo().toLocaleUpperCase()).subscribe(text =>{this.featuresTitle = text;})
                })
                this.innerWidth = (window.screen.width);
  }

  ngOnInit() {
    this.session.setCurrentInfo('landlord');
    setTimeout(_=>{
      this.hideHome = false;
    }, 50)
  }

  openStore(platform){
    if(platform == "ios"){
      window.open("https://itunes.apple.com/us/app/livingsqr-property-management/id1387337952?ls=1&mt=8", '_blank');
    }
    if(platform == "android"){
      window.open("https://play.google.com/store/apps/details?id=com.ionicframework.livingsqr", '_blank');
    }
  }
  
  scrollToId(event){
    if (event){
      this.fragment = event;
    }else{
      this.fragment = this.session.getRouteFragment();
    }
    if(this.fragment){
      document.querySelector('#' + this.fragment).scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
    }
  }

  isTenant(){
    return this.session.getCurrentInfo() == 'tenant';
  }

  userIsTenant(){
    this.session.setCurrentInfo("tenant");
    //this.featuresTitle = "Tenant";
    this.translate.get("HOME." +this.session.getCurrentInfo().toLocaleUpperCase()).subscribe(text =>{this.featuresTitle = text;})
  }

  userIsLandlord(){
    this.session.setCurrentInfo('landlord');
    //this.featuresTitle = "Landlord";
    this.translate.get("HOME." +this.session.getCurrentInfo().toLocaleUpperCase()).subscribe(text =>{this.featuresTitle = text;})
  }

}
