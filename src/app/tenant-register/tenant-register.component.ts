import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {TranslateService} from 'ng2-translate';
import {EmailHandlerService} from '../email-handler.service';

@Component({
  selector: 'app-tenant-register',
  templateUrl: './tenant-register.component.html',
  styleUrls: ['./tenant-register.component.scss']
})
export class TenantRegisterComponent implements OnInit {

  registerForm: FormGroup;
  successfullySent = false;
  emailSentTo = "";
  submit_once = false;
  landlordName = "";
  tenantName = "";

  loading = false;
  emailMessage = "";

  constructor(public emailHandler: EmailHandlerService,
              public translate: TranslateService) {

    this.registerForm = new FormGroup({
      'email': new FormControl('', [Validators.required]),
      'tenant_name': new FormControl('', [Validators.required]),
      'landlord_name': new FormControl('', [Validators.required]),
      'landlord_language': new FormControl('en', [Validators.required])
    });

    this.translate.get('EMAIL.TENANT_REG', {landlord_name: 'landlord_name', tenantName: "tenant_name"}).subscribe(data => {
      this.emailMessage = data;
    });
    this.translate.onLangChange.subscribe(_=>{
      this.translate.get('EMAIL.TENANT_REG', {landlord_name: 'landlord_name', tenantName: "tenant_name"}).subscribe(data => {
        this.emailMessage = data;
      });
    })
  }

  ngOnInit() {
  }

  onSubmit(){
    this.submit_once = true;
    if(this.registerForm.valid){
      let data = {
        landlord_name: this.landlordName,
        tenant_name: this.tenantName,
        email: this.registerForm.get('email').value,
        language: this.registerForm.get('landlord_language').value
      }
      this.loading = true;
      this.emailHandler.registerTenant(data).then(res =>{
        this.emailSentTo = this.registerForm.get('email').value;
        this.successfullySent = true;
        this.registerForm.reset();
        this.loading = false;
      }, err =>{
        this.loading = false;
      })
    }
  }

}
