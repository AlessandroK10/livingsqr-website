import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { trigger, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-sponsors',
  templateUrl: './sponsors.component.html',
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({transform: 'translateX(30%)', opacity: 0}),
          animate('500ms', style({transform: 'translateX(0)', opacity: 1}))
        ])
      ]
    )
  ],
  styleUrls: ['./sponsors.component.scss']
})
export class SponsorsComponent implements OnInit {

  imgSelect = 0;
  recursion = false;
  clicked = true;

  constructor(public router: Router) {
  }

  ngOnInit(){
    this.router.events.subscribe((val) =>{
      this.recursion = false;
    })
    this.recursion = true;
    this.changeImg();
  }

  activateControl(selector){
    this.clicked = true;
    this.imgSelect = selector;
  }

  changeImg(){
    setTimeout(_=>{
      if(!this.clicked){
        this.imgSelect = this.imgSelect == 0 ? 1 : 0;
      }else{
        this.clicked = false;
      }
      if(this.recursion){
        this.changeImg();
      }
    }, 7500)
  }


}
