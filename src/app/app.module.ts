import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Http, HttpModule } from '@angular/http';
import {TranslateModule, TranslateStaticLoader, TranslateLoader} from 'ng2-translate';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { HomeComponent } from './home/home.component';
import { LandlordRegisterComponent } from './landlord-register/landlord-register.component';
import { TenantRegisterComponent } from './tenant-register/tenant-register.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { ValidateComponent } from './validate/validate.component';

import {ApiService} from './api.service';
import {EmailHandlerService} from './email-handler.service';
import {SessionService} from './session.service';
import { FeatureOverviewComponent } from './feature-overview/feature-overview.component';
import { PaymentInfoComponent } from './payment-info/payment-info.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { FooterComponent } from './footer/footer.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { AboutComponent } from './about/about.component';
import { SponsorsComponent } from './sponsors/sponsors.component';
import { SponsorFormComponent } from './sponsor-form/sponsor-form.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TermsComponent } from './terms/terms.component';
import { GetStartedComponent } from './get-started/get-started.component';
import { PricingComponent } from './pricing/pricing.component';

import {MatSliderModule} from '@angular/material/slider';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import { TryDemoComponent } from './try-demo/try-demo.component';
import { ResidenceComponent } from './residence/residence.component';

export function createTranslateLoader(http: Http) {
  return new TranslateStaticLoader(http, './assets/i18n', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LandlordRegisterComponent,
    TenantRegisterComponent,
    PasswordResetComponent,
    ValidateComponent,
    FeatureOverviewComponent,
    PaymentInfoComponent,
    PageNotFoundComponent,
    FooterComponent,
    ContactUsComponent,
    NavBarComponent,
    AboutComponent,
    SponsorsComponent,
    SponsorFormComponent,
    PrivacyPolicyComponent,
    TermsComponent,
    GetStartedComponent,
    PricingComponent,
    TryDemoComponent,
    ResidenceComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    TranslateModule.forRoot({
      provide: TranslateLoader,
      useFactory: (createTranslateLoader),
      deps: [Http]
    }),
    MatSliderModule, MatInputModule, MatButtonModule
  ],
  providers: [
    ApiService,
    EmailHandlerService,
    SessionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
