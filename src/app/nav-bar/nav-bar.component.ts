import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { trigger, style, animate, transition } from '@angular/animations';
import {TranslateService} from 'ng2-translate';
import { SessionService } from '../session.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss'],
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({opacity: 0}),
          animate('150ms', style({ opacity: 1}))
        ]),
        transition(':leave', [
          style({opacity: 1}),
          animate('150ms', style({ opacity: 0}))
        ])
      ]
    )
  ]
})
export class NavBarComponent implements OnInit {

  @Output() scrollChange: EventEmitter<string> = new EventEmitter<string>();
  @Input() isBlack:boolean;


  innerWidth: any;
  showMobileMenu = false;

  constructor(private router: Router, 
              public session: SessionService,
              public translate: TranslateService) {
    this.innerWidth = (window.screen.width);
  }

  ngOnInit() {
    this.scrollToId(null);
  }

  scrollToId(fragment:string){
    this.turnOffMenu();
    this.scrollChange.emit(fragment);
    this.session.setRouteFragment(fragment);
  }

  goHome(){
    this.router.navigateByUrl("/");
  }

  toggleMobileMenu(){
    this.showMobileMenu = !this.showMobileMenu;
  }

  turnOffMenu(){
    this.showMobileMenu = false;
  }

  changeLang(language: string){
    this.translate.use(language);
  }

  changeLangToggle(){
    this.turnOffMenu();
    if(this.translate.currentLang == ("en")){
      this.translate.use("fr");
    }else{
      this.translate.use("en");
    }
  }

}
