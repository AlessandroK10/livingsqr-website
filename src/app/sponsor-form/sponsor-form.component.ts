import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {EmailHandlerService} from '../email-handler.service';

@Component({
  selector: 'app-sponsor-form',
  templateUrl: './sponsor-form.component.html',
  styleUrls: ['./sponsor-form.component.scss']
})
export class SponsorFormComponent implements OnInit {

  registerForm: FormGroup;
  successfullySent = false;
  emailSentTo = "";
  submit_once = false;

  loading = false;

  constructor(public emailHandler: EmailHandlerService) { 
    this.registerForm = new FormGroup({
      'company_name': new FormControl('', [Validators.required]),
      'contact_email': new FormControl('', [Validators.required]),
      'country': new FormControl('', [Validators.required]),
      'city': new FormControl('', [Validators.required]),
      'address': new FormControl('', [Validators.required]),
      'website': new FormControl('', []),
      'comments': new FormControl('', [])
    });
  }

  ngOnInit() {
  }

  onSubmit(){
    this.submit_once = true;
    if(this.registerForm.valid){
      let data = {
        sponsor_name: this.registerForm.get('company_name').value,
        email: this.registerForm.get('contact_email').value,
        website: this.registerForm.get('website').value ? this.registerForm.get('website').value : " ",
        country: this.registerForm.get('country').value,
        city: this.registerForm.get('city').value,
        address: this.registerForm.get('address').value,
        comments: this.registerForm.get('comments').value
      };
      this.loading = true;
      this.emailHandler.registerSponsor(data).then(res =>{
        this.emailSentTo = this.registerForm.get('contact_email').value;
        this.successfullySent = true;
        this.loading = false;
      }, err =>{
        this.loading = false;
        console.log(err);
      })
    }
  }

}
