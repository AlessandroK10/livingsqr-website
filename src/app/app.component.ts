import { Component, OnInit } from '@angular/core';
import { Title }  from '@angular/platform-browser';
import { Router, NavigationEnd } from '@angular/router';
import {TranslateService} from 'ng2-translate';
import * as AOS from 'aos';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'app';
  userLang = navigator.language  
  constructor(private router: Router,
              private translate: TranslateService,
              public titleService: Title) {
    translate.setDefaultLang("en");   
    if(this.userLang.substr(0,2) == "fr"){
      translate.use('fr');
    }else{
      translate.use('en');
    } 
  }

  ngOnInit() {
    AOS.init();
    this.router.events.subscribe((evt) => {
        if(evt['url'] && evt['url'][1]){
          let extension = "";
          if(evt['url'] == "/partners-sponsors"){
            extension = "Partners";
          }else if(evt['url'] == "/get-started"){
            extension = "Get Started"
          }else{
            extension = evt['url'][1].toUpperCase() + evt['url'].substring(2); 
            let matcher = extension.match(/^([a-zA-Z0-9].*?\b).*$/)[1];
            if(matcher){
              extension = matcher;
            }
          }
          this.titleService.setTitle("LivingSqr - " + extension);// extension[1].toUpperCase() + extension.substring(2)
        }
        if (!(evt instanceof NavigationEnd)) {
            return;
        }
        window.scrollTo(0, 0)
    });
  }
}

