import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {EmailHandlerService} from '../email-handler.service';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss']
})
export class PasswordResetComponent implements OnInit {

  id = "";
  status = "";
  complete = "";
  newPass = "";
  confirmNewPass = "";
  user = null;

  constructor(private emailHandler: EmailHandlerService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.emailHandler.isPasswordRequestValid(this.id).then(user =>{
      this.status = "valid";
      this.user = user;
    }, err =>{
      this.status = "expired";
    })
  }


  saveNewPassword(){
    if(this.newPass && this.newPass == this.confirmNewPass){
      this.emailHandler.resetPassword(this.user.email, this.newPass).then(user =>{
        this.complete = "done";
      }, err =>{
        this.complete = "error";
      })
    }else{
      //error: passwords dont match
      this.complete = "form-error"
    }
  }

}
