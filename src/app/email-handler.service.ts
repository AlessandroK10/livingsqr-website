import { Injectable } from '@angular/core';
import {ApiService} from './api.service';
import { reject } from 'q';

@Injectable()
export class EmailHandlerService {

  constructor(public api: ApiService) { }


  validateEmail(email_validation_code: string){
    return new Promise((resolve, reject) =>{
      this.api.post({code: email_validation_code},"validate_email").then(res =>{
        resolve(res);
      }, err =>{
        console.log(err);
        reject(err);
      })
    })
  }

  isPasswordRequestValid(password_reset_code: string){
    return new Promise((resolve, reject) =>{
      this.api.post({code: password_reset_code}, "is_password_reset_valid").then(user =>{
        resolve(user);
      }, err =>{
        reject(err);
      })
    })
  }

  resetPassword(email, newPassword){
    return new Promise((resolve, reject) =>{
      this.api.post({email: email, password: newPassword}, "reset_password").then(user =>{
        resolve(user);
      }, err =>{
        reject(err);
      })
    })
  }

  registerLandlord(params){
    return new Promise((resolve, reject)=>{
      this.api.post(params, "register_landlord").then(res =>{
        resolve(res);
      }, err =>{
        reject(err);
      })
    })
  }

  registerTenant(params){
    return new Promise((resolve, reject) =>{
      this.api.post(params, "register_tenant").then(res =>{
        resolve(res);
      }, err =>{
        reject(err);
      })
    })
  }

  registerSponsor(params){
    return new Promise((resolve, reject) =>{
      this.api.post(params, "register_sponsor").then(res =>{
        resolve(res);
      }, err =>{
        reject(err);
      })
    })
  }

  contactUs(params){
    return new Promise((resolve, reject) =>{
      this.api.post(params, "contact_us").then(res =>{
        resolve(res);
      }, err =>{
        reject(err);
      })
    })
  }

}
