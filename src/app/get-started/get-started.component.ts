import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-get-started',
  templateUrl: './get-started.component.html',
  styleUrls: ['./get-started.component.scss']
})
export class GetStartedComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  openStore(platform){
    if(platform == "ios"){
      window.open("https://itunes.apple.com/us/app/livingsqr-property-management/id1387337952?ls=1&mt=8", '_blank');
    }
    if(platform == "android"){
      window.open("https://play.google.com/store/apps/details?id=com.ionicframework.livingsqr", '_blank');
    }
  }

}
